var jade = require('./runtime');

module.exports = function (locals, attrs, escape, rethrow, merge
/**/) {
attrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;
var buf = [];
with (locals || {}) {
var interp;
buf.push('<!DOCTYPE html><html lang="en"><head><title>Index Page</title><link rel="stylesheet" href="css/styles.css"><script type="text/javascript" src="js/build/production.min.js"></script></head><body><h1>Jade - node template engine</h1><div id="container" class="col"><p>Get on it!</p></div><p>Jade is a terse and simple\ntemplating language with a\nstrong focus on performance\nand powerful features.</p></body></html>');
}
return buf.join("");
};