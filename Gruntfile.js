/// <binding BeforeBuild='default' />
module.exports = function(grunt) {

    // 1. Вся настройка находится здесь
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            // 2. Настройка для объединения файлов находится тут
			dist: {
				src: [
					'js/libs/*.js', // Все JS в папке libs
					'js/global.js'  // Конкретный файл
				],
				dest: 'js/build/production.js',
			}
        },
		
		uglify: {
			build: {
				src: 'js/build/production.js',
				dest: 'js/build/production.min.js'
			}
		},
		
		imagemin: {
			dynamic: {
				files: [{
					expand: true,
					cwd: 'img/',
					src: ['**/*.{png,jpg,gif}'],
					dest: 'images/'
				}]
			}
		},
		
		stylus: {
			compile: {
				options: {
					paths: ['css/stylus'],
					urlfunc: 'data-uri', // use data-uri('test.png') in our code to trigger Data URI embedding
				},
				files: {
					'css/styles.css': ['css/stylus/*.styl'] // compile and concat into single file
				}
			}
		},
		
		jade: {
			compile: {
				options: {
					data: {
						debug: false
					}
				},
				files: {
					"index.html": ["jade/index.jade"]
				}
			}
		},
		
		watch: {
			options: { livereload: true, },
			scripts: {
				files: ['js/*.js'],
				tasks: ['concat', 'uglify'],
				options: {
					spawn: false,
				},
			},
			stylus: {
				files: ['css/stylus/*.*'],
				tasks: ['stylus:compile'],   // This needs to be "tasks" (not "task")
				options: {
					spawn: false,
				},
			},
			jade: {
				files: ['jade/*.*'],
				tasks: ['jade'],   // This needs to be "tasks" (not "task")
				options: {
					spawn: false,
				},
			},
			imagemin: {
				files: ['img/*.*'],
				tasks: ['imagemin'],   // This needs to be "tasks" (not "task")
				options: {
					spawn: false,
				},
			},
		}

    });

    // 3. Тут мы указываем Grunt, что хотим использовать этот плагин
    grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-stylus');
	grunt.loadNpmTasks('grunt-contrib-jade');
	grunt.loadNpmTasks('grunt-contrib-watch');

    // 4. Указываем, какие задачи выполняются, когда мы вводим «grunt» в терминале
    grunt.registerTask('default', ['concat', 'uglify', 'imagemin', 'stylus', 'jade', 'watch']);

};